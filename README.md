# python-challenge



## Getting started

I thought about docker it but I run out of time that I set myself. On the other hand - is it really necessary? 

##### What do you need to do to run it?

Well, nothing much - some virtual environment. I didn't use any fancy DB to keep things easy. `SQLite` did its job.

So I assume you have i.e. `conda`. I will use it with Python 3.8.

```
git clone https://gitlab.com/mikex/python-challenge.git 

cd python-challenge

conda create --name python-challenge python=3.8
conda activate python-challenge

pip install -r requirements/dev.txt

cd sw_explorer

python manage.py migrate
python manage.py runserver
```

Now you're good to go to visit http://127.0.0.1:8000/.

## What can I find there?

* http://127.0.0.1:8000/ <- here you can see list of people collections/fetch a new one.
* http://127.0.0.1:8000/PK/ <- detail view of the collection with table, load more and count functionalities.
* http://127.0.0.1:8000/PK/count/ <- you will be redirected here from detail view - it's basically the count values functionality.

It's not pixel perfect but I tried at least somehow mirror the images from pdf with description of the challenge with Bootstrap :)

## Ok, cool, but what's missing?

* Tests.

I was working on it very irregularly and I just didn't have enough time. Shame.

## Interesting facts

* https://swapi.co/ is now https://swapi.dev/

Obviously the running time depends in direct proportion to the swapi latency.

When I first fetched the first page od people's API it took about 13s. I was pretty much terrified of it even though it's probably a one-time incident.

So I thought it must be paralleled. That's why I call the API, get count of elements, calculate the pages and prepare links.

Final results on my machine:
```
fetch all people results: 1.2428739070892334s
cut only needed data: 1.0967254638671875e-05s
transformation from challenge: 5.125999450683594e-05s
save as csv:  2.893589973449707s
``` 

![Not great, not terrible](https://c.tenor.com/wtg92v8U6fMAAAAC/chernobyl-not-great.gif)

I guess.
