from django.conf import settings

SWAPI_URL = getattr(settings, "SWAPI_URL", "https://swapi.dev/api")

# mapping of resources of swapi and corresponding endpoint
RESOURCES = {
    "films": "films",
    "people": "people",
    "planets": "planets",
    "species": "species",
    "starships": "starships",
    "vehicles": "vehicles",
}
