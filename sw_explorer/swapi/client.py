import math
from typing import List, Optional

import requests
from django.core.exceptions import ValidationError
from swapi import enums
from swapi.helpers import FetchSiteHelper


class SWAPIClient:
    def __init__(self, session: Optional[requests.Session] = None):
        self.session = session or requests.Session()
        self.cache = (
            {}
        )  # simple dict cache to save some extra requests; TTL as long as the instance

    def fetch_all(self, resource: str) -> List:
        if resource not in enums.RESOURCES:
            raise ValidationError(
                f"Unknown resource. Make sure you're watching for one of {list(enums.RESOURCES.keys())}"
            )

        sites = self._prepare_urls(resource)
        fetch_helper = FetchSiteHelper(self.session)
        results = fetch_helper.fetch_all_sites(sites)

        return results

    def retrieve_specific_attribute_from_url(self, attribute: str, url: str):
        if not self.cache.get(url):
            response = self.session.get(url)
            response.raise_for_status()
            response_json = response.json()
            self.cache[url] = response_json

        return self.cache[url][attribute]

    def get_count(self, resource: str) -> int:
        url = f"{enums.SWAPI_URL}/{resource}/"

        return self.retrieve_specific_attribute_from_url("count", url)

    def get_results_per_page(self, resource: str) -> int:
        url = f"{enums.SWAPI_URL}/{resource}/"

        return len(self.retrieve_specific_attribute_from_url("results", url))

    def _prepare_urls(self, resource) -> List:
        count = self.get_count(resource)
        results_per_page = self.get_results_per_page(resource)
        pages = math.ceil(count / results_per_page)

        return [f"{enums.SWAPI_URL}/{resource}/?page={i}" for i in range(1, pages + 1)]
