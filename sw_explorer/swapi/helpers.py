import threading
from concurrent.futures import ThreadPoolExecutor
from typing import Optional

import requests


class FetchSiteHelper:
    """
    Helper class to fetch single url or given in list.

    Could use `asyncio`/`aiohttp` but the challenge specifically requested `requests`.
    As far as I remember `requests.Session` is not thread-safe - this is why
    I used "local" storage `threading.local()`.
    """

    def __init__(self, session: Optional[requests.Session] = None):
        self._session = session or requests.Session()
        self._thread_local = threading.local()
        self._thread_local.session = self._session

        self.results = []

    def fetch_site(self, url, return_value=False):
        with self._session.get(url) as response:
            response.raise_for_status()

            data = response.json()
            self.results.append(data)

            if return_value:
                return data

    def fetch_all_sites(self, sites):
        # results probably could be gathered using concurrent.futures API
        with ThreadPoolExecutor() as executor:
            executor.map(self.fetch_site, sites)

        return self.results
