from django.db import models


class CollectionBase(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    description = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True


class People(CollectionBase):
    UPLOAD_TO = "collections/people/"

    file_name = models.CharField(max_length=70, blank=True)
    collection_file = models.FileField(upload_to=UPLOAD_TO)

    def __str__(self):
        return f"People collection: {self.file_name} ({self.created_at})"
