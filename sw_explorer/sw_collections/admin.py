from django.contrib import admin
from sw_collections.models import People


class PeopleAdmin(admin.ModelAdmin):
    list_display = [
        "file_name",
        "created_at",
    ]


admin.site.register(People, PeopleAdmin)
