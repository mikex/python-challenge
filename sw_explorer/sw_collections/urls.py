from django.urls import path
from sw_collections.views import (
    PeopleCollectionCountView,
    PeopleCollectionDetailView,
    PeopleCollectionListView,
)

urlpatterns = [
    path("", PeopleCollectionListView.as_view(), name="people-collection-list"),
    path(
        "<int:pk>/",
        PeopleCollectionDetailView.as_view(),
        name="people-collection-detail",
    ),
    path(
        "<int:pk>/count/",
        PeopleCollectionCountView.as_view(),
        name="people-collection-count",
    ),
]
