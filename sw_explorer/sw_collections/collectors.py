import os
import uuid
from typing import List

import petl
from django.conf import settings
from sw_collections.models import People
from swapi.client import SWAPIClient


class PeopleCollector:
    def __init__(self):
        self.client = SWAPIClient()

    def collect(self, save_as_csv: bool = True) -> petl.Table:
        people_dataset: List[dict] = self.fetch_dataset()
        table: petl.Table = self.transform_dataset(people_dataset)

        if save_as_csv:
            self._save_collection_as_csv(table)

        return table

    def fetch_dataset(self) -> List[dict]:
        all_people = [
            character
            for page in self.client.fetch_all("people")
            for character in page["results"]
        ]
        return all_people

    def transform_dataset(self, dataset: List[dict]) -> petl.Table:
        table: petl.Table = petl.fromdicts(dataset)

        table = petl.cutout(
            table, "films", "species", "vehicles", "starships", "created", "url"
        )
        table = petl.rename(table, "edited", "date")
        table = petl.convert(table, "date", lambda v: self._reformat_datetime(v))
        table = petl.convert(
            table, "homeworld", lambda v: self._retrieve_homeworld_name(v)
        )

        return table

    def _reformat_datetime(self, d: str) -> str:
        return petl.datetimeparser("%Y-%m-%dT%H:%M:%S.%fZ")(d).strftime("%Y-%m-%d")

    def _retrieve_homeworld_name(self, url: str):
        return self.client.retrieve_specific_attribute_from_url("name", url)

    def _save_collection_as_csv(self, collection: petl.Table):
        file_name = f"{uuid.uuid4().hex}.csv"
        upload_path = os.path.join(settings.MEDIA_ROOT, People.UPLOAD_TO)
        file_path = os.path.join(upload_path, file_name)

        if not os.path.exists(upload_path):
            os.makedirs(upload_path)

        petl.tocsv(collection, file_path)
        return People.objects.create(file_name=file_name, collection_file=file_path)
