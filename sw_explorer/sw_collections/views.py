import petl
from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic import DetailView, ListView
from sw_collections.collectors import PeopleCollector
from sw_collections.models import People


class PeopleCollectionListView(ListView):
    queryset = People.objects.all().order_by("-created_at")
    template_name = "sw_explorer/people_collection_list.html"

    def post(self, request):
        collector = PeopleCollector()

        try:
            collector.collect()
        except Exception as e:  # PeopleCollector/FetchSiteHelper should handle exceptions/errors
            messages.error(request, "Couldn't fetch new collection.")
        else:
            messages.success(request, "Fetching succeeded!")

        return redirect("/")


class PeopleCollectionDetailView(DetailView):
    model = People
    template_name = "sw_explorer/people_collection_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            show_rows = int(self.request.GET.get("rows"))
        except (ValueError, TypeError):
            show_rows = 10

        # Probably should be taken out to some helper/util
        table: petl.Table = petl.fromcsv(self.object.collection_file)

        context["header"] = petl.header(table)
        context["rows"] = petl.data(petl.head(table, show_rows))
        context["rows_displayed_count"] = len(
            context["rows"]
        )  # part of load more - probably should be rethinked
        context["rows_all_count"] = petl.nrows(table)

        return context


class PeopleCollectionCountView(DetailView):
    model = People
    template_name = "sw_explorer/people_collection_count.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Probably should be taken out to some helper/util
        table: petl.Table = petl.fromcsv(self.object.collection_file)
        selected_headers = self.request.GET.getlist("selected_headers")

        # a little magic to show headers before or after aggregation
        if not selected_headers:
            context["headers_to_count"] = petl.header(table)
        else:
            key = selected_headers if len(selected_headers) > 1 else selected_headers[0]
            aggregated_table = petl.aggregate(
                table, key=key, aggregation={"count": len}
            )
            context["rows"] = petl.data(aggregated_table)
            context["header"] = petl.header(aggregated_table)

        return context
